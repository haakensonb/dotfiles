" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'

Plug 'scrooloose/nerdtree'

Plug 'airblade/vim-gitgutter'

Plug 'mattn/emmet-vim'

Plug 'scrooloose/nerdcommenter'

Plug 'vimwiki/vimwiki'

" Initialize plugin system
call plug#end()

set t_Co=256

set background=dark

colorscheme gruvbox

set number

inoremap jj <Esc>

map <C-n> :NERDTreeToggle<CR>

autocmd FileType javascript setlocal shiftwidth=2 tabstop=2

autocmd FileType css setlocal shiftwidth=2 tabstop=2

autocmd FileType c setlocal shiftwidth=2 tabstop=2

autocmd FileType cpp setlocal shiftwidth=2 tabstop=2

autocmd FileType html setlocal shiftwidth=4 tabstop=4

set expandtab

filetype plugin indent on
